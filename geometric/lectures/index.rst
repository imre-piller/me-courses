Geometric Modeling
==================

.. include:: pages/00_Overview.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:

   pages/01_Curves
   pages/02_Interpolation
   pages/03_Bezier
   pages/04_Splines
   pages/05_Racional
   pages/06_Surfaces
   pages/07_Patches
   pages/08_CSG
   pages/09_Volumetric
   pages/10_CV

