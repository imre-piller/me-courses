Overview
========

The topic of geometric modeling focusing on the description of various shapes in different dimensional spaces. Its main two topics are the curves and the surfaces.

We can distinguished open and closed curves and surfaces.

The examination of the join points of the curves is an important part of the topic. It ranks the continuity of the curves at the given point.


Purpose of the topic
--------------------

:math:`\rhd` Why are these topics are necessary for software engineers?

* Industrial design and applications related to mechanics
* Design of visual elements in 2D and 3D
* Designing animations
* Usage of vector graphical formats
* Font faces
* Space informatics, mapping
* Computer Vision


Literature
----------

* Jean Gallier: *Curves and Surfaces in Geometric modelling: Theory and Algorithms*, Department of Computer and Information Science, University of Pennsylvania, 2025.
* G. Farin, J. Hoschek, M.-S. Kim: *Handbook of Computer Aided Geometric Design*, Elsevier, 2002.

