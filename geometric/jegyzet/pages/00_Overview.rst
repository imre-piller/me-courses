Áttekintés
----------

A geometriai modellezés a különféle alakzatok leírási módjával foglalkozik különböző dimenziós terekben. A tárgyköre elsősorban a görbék és a felületek modellezésére összpontosít.

Görbék és felületek kapcsán is megkülönböztethetünk nyitott és zárt változatokat.

A témakör igen fontos része az illeszkedések vizsgálata, amely különböző rendű folytonosságokat vizsgál.


A tárgy szerepe
---------------

:math:`\rhd` Miért lehet szüksége egy informatikusnak a tárgyra?

* Formatervezéshez és gépészethez köthető alkalmazások
* Vizuális elemek tervezése 2D és 3D esetén
* Animációk készítése
* Vektorgrafikus formátumok ismerete
* Betűtípusok
* Térinformatika, térképészet
* Képfeldolgozás


Irodalom
--------

* Juhász Imre: Görbék és felületek modellezése, jegyzet, 2020 (https://geometria.uni-miskolc.hu/files/23577/GFM.zip)
* Szirmay-Kalos László, Antal György, Csonka Ferenc: Háromdimenziós grafika, animáció és játékfejlesztés, ComputerBooks, 2001, (https://cg.iit.bme.hu/~szirmay/3Dgraf.pdf)

