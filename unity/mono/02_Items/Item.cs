namespace Classes
{
    public class Item
    {
        public bool IsValid {get; set;}

        public Item(bool isValid)
        {
            this.IsValid = isValid;
        }
    }
}

