# Szkriptek írása

Példa:
* Hozzunk létre egy új objektumot!
* Hozzunk létre egy szkriptet a projekten belül!
* Rendeljük hozzá az objektumhoz a szkriptet.

## A `GameObject`

## `MonoBehaviour`

```csharp
Start();
Update();
Debug.Log();
```

A publikus változók látszanak az *Inspector*-ban.

## Komponens tulajdonságok és műveletek
