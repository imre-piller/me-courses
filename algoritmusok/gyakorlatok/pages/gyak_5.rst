5. Dinamikus adatszerkezetek és műveleteik
==========================================

Tömb
----

Jellemző műveletei:

* Keresés
* Beszúrás
* Törlés

Feladatok:

* Vizsgáljuk meg a lineáris keresés algoritmusát!
* Mutatók (*pointerek*)
* Nézzük meg az egyes műveletek számítási idejét!
* Vizsgáljuk meg, hogy hogyan zajlik a dinamikus memóriakezelés a tömb átméretezése közben!

*Bináris keresés*

* Milyen feltételezésünk van a használatához?
* Vizsgáljuk meg az alábbi példán keresztül a működését!

Mátrixok
--------

* Hogyan oldható meg a címzés 2 (vagy több) index segítségével!
* Milyen esetek fordulhatnak elő! Adjuk meg hozzá a számítási módot!

Lista
-----

A jellemző műveletei ugyanazok, mint a tömbnek.

* Egyszeresen láncolt
* Duplán láncolt
* Szentineles láncolt lista
* Hasonlítsuk össze a tömb és a lista műveleteinek a számítási igényeit (a speciális eseteket figyelembe véve)!

Verem
-----

*Stack*

* FILO, LIFO
* Reprezentációk

Sor
---

*Queue*

* FIFO, LILO
* Reprezentációk

Fa struktúrák
-------------

* Hogyan tárolhatjuk le egy teljes bináris fának az elemeit egy tömbben?
* Hogyan készíthetünk dinamikusan tetszőleges bináris fákat?
