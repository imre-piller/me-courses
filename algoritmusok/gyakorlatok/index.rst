Adatstruktúrák és algoritmusok
==============================

.. toctree::
   :maxdepth: 2
   :caption: Tartalomjegyzék

   pages/gyak_1
   pages/gyak_2
   pages/gyak_3
   pages/gyak_4
   pages/gyak_5
   pages/gyak_6
   pages/gyak_7
   pages/gyak_8
   pages/gyak_9
   pages/gyak_10
   pages/gyak_11
