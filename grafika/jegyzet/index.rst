.. Számítógépi grafika jegyzet, Piller Imre, 2024.

Számítógépi grafika
===================

.. toctree::

   pages/overview
   pages/literature
   pages/assignment
   pages/development
   pages/faq


.. toctree::
   :maxdepth: 2
   :caption: Tartalomjegyzék
   :numbered:

   pages/topics/01_Coordinates
   pages/topics/02_Devices
   pages/topics/03_Algorithms
   pages/topics/04_Projections
   pages/topics/05_Pipeline
   pages/topics/06_Shading
   pages/topics/07_Textures
   pages/topics/08_Animations
   pages/topics/09_Raytracing
   pages/topics/10_Libraries
   pages/topics/11_Effects
   pages/topics/12_Texts
   pages/topics/13_Optimization
   pages/topics/14_Researches

