A tárgyról
==========

Célok
-----

* Az interaktív grafikus rendszerek működésének bemutatása.
* A grafikus megjelenítéssel kapcsolatos elméleti ismeretek áttekintése, gyakorlása.
* A grafikus alkalmazások fejlesztésének megismertetése egy saját alkalmazás fejlesztésén keresztül.
* Asset-ek (például textúrák, modellek) keresése, olyan szintű átalakítása, hogy az a programban használható legyen.
* A C programozási nyelv, objektum orientált szemlélet, a verziókövetés, az alkalmazások specifikálásának és fejlesztésének gyakorlása.

.. warning::

  Hogy aki bármilyen olyan témával találkozna, amelyről úgy gondolja, hogy nem tartozik a képzéséhez, vagy a későbbiekben nem lehet rá szüksége, az kérem, hogy jelezze!


Nem célok
---------

* Az aktuális technológiák használata, elsajátítása.
* Modellek, textúrák készítése.

.. note::

  Attól, hogy ezek nem célok, mindenkit arra biztatnék, hogy ismerkedjen meg ezekkel, foglalkozzon velük.


A jegyzet felépítése
--------------------

A jegyzetben az első 8 témakör az, amely a vizsga témaköreihez tartozik. Szintén ezekhez tartoznak tematikus gyakorlatok. A jegyzet ezen részei a következő elemekre bonthatók:

* Elméleti áttekintés ábrákkal, példákkal, számításokkal, mintaprogramokkal illusztrálva,
* kérdések a tárgykörből, melyek a vizsgafeladatok elméleti részét adják,
* számítási feladatok, melyek a vizsgafeladatok gyakorlati részét adják,
* programozási feladatok, melyek a gyakorlatokon elvégzendő feladatokhoz adnak ötleteket.

A 9-14. témakörök kitekintő jellegűek. Az azokban szereplő kérdések és feladatok inkább csak gondolatébresztő szereppel bírnak, érdekességként kerültek bele az anyagba.

