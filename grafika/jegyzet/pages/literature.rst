Ajánlott irodalmak
==================

* Juhász Imre, Lajos Sándor: *Számítógépi grafika*, 2007, https://geometria.uni-miskolc.hu/files/23578/Szamitogepigrafika.pdf
* Juhász Imre: *OpenGL*, mobiDIÁK könyvtár, 2003, https://gyires.inf.unideb.hu/mobiDiak/Juhasz-Imre/OpenGL/OpenGL.pdf
* Szirmay-Kalos László, Antal György, Csonka Ferenc: *Háromdimenziós grafika, animáció és játékfejlesztés*, 2003, https://cg.iit.bme.hu/~szirmay/3Dgraf.pdf
* Steve Marschner, Peter Shirley: *Fundamentals of Computer Graphics*, CRC Press, 2016.
* James D. Foley, Andries van Dam, Steven K. Feiner, John Hughes, Morgan McGuire, David F. Sklar, Kurt Akeley: *Computer Graphics: Principles and Practice*, Addison-Wesley, 1990.
* Samuel R. Buss: *3D Computer Graphics: A Mathematical Introduction with OpenGL*, Cambridge University Press, 2003, https://mathweb.ucsd.edu/~sbuss/MathCG/
* SDL2 könyvek, leírások: https://wiki.libsdl.org/SDL2/Books
* Brian W. Kernigham, Dennis M. Ritchie: *The C Programming Language*, 2002.

