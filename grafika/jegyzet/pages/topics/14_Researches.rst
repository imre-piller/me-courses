Aktuális kutatások és eredményeik
=================================

.. TODO: Hivatkozni a tudományos cikkekre!

Néhány aktuálisan kutatott terület a számítógépi grafika tárgykörén belül:

* Gaussian Splatting, https://repo-sam.inria.fr/fungraph/3d-gaussian-splatting/
* Light Paths
* Nanite Engine

