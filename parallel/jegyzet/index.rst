Párhuzamos algoritmusok
=======================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pages/bsc/topic_01
   pages/bsc/topic_02
   pages/bsc/topic_03
   pages/bsc/topic_04
   pages/bsc/topic_05
   pages/bsc/topic_06
   pages/bsc/topic_07
   pages/bsc/topic_08
   pages/bsc/topic_09
   pages/bsc/topic_10
   pages/bsc/topic_11
   pages/bsc/topic_12
   pages/bsc/topic_13
   pages/bsc/topic_14


Irodalom
--------

* Párhuzamos algoritmusok webes jegyzet: `https://gyires.inf.unideb.hu/KMITT/c08/index.html <https://gyires.inf.unideb.hu/KMITT/c08/index.html>`_
* Iványi Antal: Párhuzamos algoritmusok: `https://www.inf.elte.hu/dstore/document/287/Parhuzamos-algoritmusok.pdf <https://www.inf.elte.hu/dstore/document/287/Parhuzamos-algoritmusok.pdf>`_


Korábbi tárgyak anyagai
-----------------------

Levelezős MSc (2021/22. I. félév)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. toctree::
  :maxdepth: 2
  :caption: Contents:

  pages/msc/index


BSc gyakorlati anyagok (2019/20. II. félév)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. toctree::
  :maxdepth: 2
  :caption: Contents:

  pages/bsc_19_20_2/index
