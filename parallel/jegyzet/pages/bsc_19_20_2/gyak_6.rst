6. gyakorlat
============

Feladatok
---------

* Egyeztetések azzal kapcsolatban, hogy ki, mire jutott.
* A korábbi MultiPascal-os feladatok áttekintése.
* SPINLOCK és CHANNEL használatának gyakorlása.
* A korábbi gyakorlatokon csak említett, a gyakorlati diasorban szereplő plusz feladatok megoldása.
* Kérdések, észrevételek a MultiPascal-lal kapcsolatban.