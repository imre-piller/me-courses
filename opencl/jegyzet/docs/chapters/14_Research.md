# Tudományos kutatások és eredményeik

* Hardveres fejlesztések
* OpenCL, mint implementációs mód tudományos számítások esetében
* "OpenCL vs CUDA" jellegű vizsgálatok
* FPGA-ra való adaptálhatóság
* Automatikus párhuzamosítás, vektorizálás
* Ütemezés, feladatkiosztás optimalizálása
* *High Performance Computing*

