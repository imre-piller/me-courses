# OpenCL-t használó alkalmazások

Előfordulása:

* [https://en.wikipedia.org/wiki/List_of_OpenCL_applications](https://en.wikipedia.org/wiki/List_of_OpenCL_applications)

OpenCL vs CUDA:

* [https://www.quora.com/Which-programs-use-OpenCL-Why](https://www.quora.com/Which-programs-use-OpenCL-Why)

NVidia példakódok:

* [https://developer.nvidia.com/opencl](https://developer.nvidia.com/opencl)

