6. Szegmentálás
===============

* https://docs.opencv.org/master/d3/db4/tutorial_py_watershed.html
* K-Means: https://en.wikipedia.org/wiki/Image_segmentation
* https://ai.stanford.edu/~syyeung/cvweb/tutorial3.html

.. todo::

   Válogassuk szét egy kép pontjait az alapján, hogy egy :math:`k \times k` méretű kernelen belül az intenzitásoknak mekkora a tartománya! (Nézzük meg előtte maguknak a tartományok méretének az eloszlását!)

